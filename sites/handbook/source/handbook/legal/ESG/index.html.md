---
layout: handbook-page-toc
title: "ESG"
description: "Information and processes related to ESG"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The Environmental, Social, and Governance (ESG) [Team](https://handbook.gitlab.com/job-families/legal-and-corporate-affairs/environmental-social-governance/) creates and maintains GitLab’s Corporate Sustainability strategy and programs. This includes ESG disclosures and public ESG reporting, identifying and prioritizing key issues to advance GitLab’s social and environmental goals, and creating partnerships with nonprofit organizations that support GitLab’s values and mission.

## ESG Strategy
Environmental, social, and governance (ESG) practices, although newly formalized in GitLab Inc.’s (“GitLab”, “we”, “our”) strategy, have been embedded into our work culture since our inception. Deeply integrated into our business philosophy, GitLab’s ESG strategy is driven by our [values](/handbook/values/) of Collaboration, Results, Efficiency, Diversity, Inclusion and Belonging, Iteration, and Transparency (CREDIT). 

In December 2022, we conducted an ESG materiality assessment to determine which ESG topics are most important to our business and our stakeholders. Through engagement with both internal and external stakeholders, we explored which ESG topics have the greatest impact on GitLab’s business, and where we have the potential to have the greatest impact on the environment, society, and our global communities. Our materiality matrix was finalized in January 2023.

In 2023, we released our inaugural [FY23 ESG Report](https://about.gitlab.com/handbook/esg/) in the Handbook. The report includes information on our key ESG focus areas, our programs and policies, acheivements to date, metrics and targets, and plans for the future. 

## ESG Request Process

### Philanthropic Requests 
Information on how GitLab Inc. supports Registered Nonprofit Organizations can be found in the [Philanthropy Policy](https://about.gitlab.com/handbook/legal/philanthropy-policy/) 

Please note that for all Philanthropic Requests, including requests for GitLab to join as a member to an association, program or organization, approval by the ESG team and CLO is required as defined by the Oversight Responsibility section of the Policy.

If you would like to submit a philanthropic request, please follow the instructions based on your request type.

#### Monetary Contributions

There are two ways that team members can submit a request for monetary support. 

1. Request funding from the ESG team to support a Registered Nonprofit Organization OR

2. Request utilizing department or TMRG  budget to support a Registered Nonprofit Organization

If you are requesting funding from the ESG team to support a Registered Nonprofit Organization, please note that at this time, we are not accepting applications. If you would like to submit a Nonprofit Organization to be considered for support in the future, please go to the [Philanthropic Requests epic](https://gitlab.com/groups/gitlab-com/-/epics/2145) and open a new issue using the [Monetary_Support Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=monetary_support).  You will be notified if there is a future opportunity. 

If you have a department or TMRG  budget that you would like to utilize to support a Registered Nonprofit Organization, please go to the [Philanthropic Requests epic](https://gitlab.com/groups/gitlab-com/-/epics/2145) and open a new issue using the [Monetary_Support Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=monetary_support). Please tag your manager to approve the request if you are submitting on behalf of your department. If you are submitting a request on behalf of a TMRG or DIB, please add Sherida McMullan as a reviewer. 

Please allow a minimum of 10 working days for review. 

#### Team Member Volunteerism
Through the GitLab Volunteer Program, GitLab team members are able to organize volunteer activities in their local community or virtually. If you would like to submit a request to organize a volunteer activity with a Registered Nonprofit Organization, please go to the [Philanthropic Requests epic](https://gitlab.com/groups/gitlab-com/-/epics/2145) and open a new issue using the [Volunteer_Support Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=volunteer_support).

**SuitUp Volunteer Opportunities**

[SuitUp](https://www.volunteersuitup.com/) is a 501c3 nonprofit that helps students prepare for life beyond the classroom and increase career readiness by partnering with businesses and schools to develop, organize, and implement engaging innovative business plan competitions in communities. SuitUp empowers students to discover their passions through experiential learning opportunities and mentorship alongside corporate volunteers.

GitLab and SuitUp are teaming up to provide virtual volunteer opportunities for team members to make an impact on students around the world. Below is a list of the upcoming volunteer opportunities with SuitUp: 

1. College & Career Panel: 
    - Overview
      - We will be kicking off the partnership with SuitUp by hosting a panel for students around the world. The panel's purpose is to share what it's like to work at GitLab while educating students about career opportunities, working in tech, individual college/career journeys, and resources to help support the journey into the corporate world.
   - Date/Time: **Wednesday, August 2 from 10-11am EST**
   - Sign up to be a panelist [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34312)

2. Virtual Competitions

   - Overview: 
       - Coached by GitLab volunteers, students (in teams of 4–10) will role-play as CEOs of the company tasked with solving a realistic corporate challenge. GitLab will work directly with SuitUp’s Programs Team to design a custom competition challenge that reflects their brand and social impact initiatives. Each team will pitch their idea to panel of judges and the winning team will come away with a prize.
   - Dates/Times: 
        - 1st Competition: **October 11, 9-11am EST**
        - 2nd Competition: **October 11, 7-9pm EST** 
   - Sign up to be a volunteer [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34178)  


#### In Kind Support & Matching Gifts Program
Currently, GitLab does not offer an in-kind product donation program for nonprofits. Please keep an eye on this page for updates. In addition, at this time, GitLab does not offer a matching gifts program. 

### Membership Requests (non-monetary)
For requests related to GitLab Membership of Association, Program or Organization, and includes terms, conditions and/or obligations on GitLab that must be executed, please follow the below process.  
Open an [issue using the Membership Request Issue Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=membership-request). Complete and attach the necessary information. Note: If you are submitting a request on behalf of a TMRG or DIB, please add Sherida McMullan as a reviewer  

NOTE: For any request(s) that require payment, please be certain to follow applicable ESG & Procurement processes.

## ESG Materiality Matrix
Six key topics were identified in GitLab’s materiality assessment. These key topics will drive GitLab’s ESG strategy and program development. This page will continue to be updated as we make progress towards developing plans and programs to advance our ESG goals.

![2023 Materiality Matrix](/handbook/legal/ESG/GitLab_2022_MaterialityMatrix_062123.PNG)

Here are GitLab’s current key topics with their drivers:

### Environment

#### Climate action and greenhouse gas emissions

* Measurement and reduction of scope 1, 2 and 3 emissions
* Greenhouse gas reduction targets, goals and commitments

### Social

#### Diversity, inclusion and belonging

* Diverse hiring and recruitment of underrepresented groups
* Culture of DIB (events, TMRGs, courses, etc.)
* Pay and promotion equity and inclusive benefits
* Board, leadership and workforce diversity KPIs
* Product accessibility

#### Talent management and engagement

* Talent attraction and recruitment
* Team member learning and development
* Leadership programs
* Succession planning
* Talent retention
* Workplace culture and remote work
* Comp and benefits

### Governance

#### Cybersecurity and data privacy

* Data, system and network breaches
* Monitoring of emerging threats
* Information security training
* Customer data use transparency
* Data processing and storage
* PII

#### Business ethics

* Up to date code of conduct, anti-corruption, anti-bribery policies
* Regulatory compliance
* Legal proceedings and monetary losses
* Internal compliance
* Oversight and ethics training
* Human rights policies, risk assessments and due diligence

#### Responsible product development

* Open source
* Ethical AI
* Human rights issues from product use
* Environmental impact considerations for product
* Data processing and storage

## Our Progress
Please read our [ESG report](https://about.gitlab.com/handbook/esg/) to learn about our progress. 

## ESG Training 
To learn more about ESG at GitLab, please take our ESG Training course available on [LevelUp](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/esg-training) and in the [Handbook](https://about.gitlab.com/handbook/legal/ESG/ESG-Training/). 

## Measuring Results 

Disclosing our progress through data aligns with our [Transparency](/handbook/values/#transparency) and [Results](/handbook/values/#results) values. We have a section in our Handbook devoted to [Key Performance Indicators](/company/kpis/) (KPIs) where we update our progress regularly. Every part of GitLab has KPIs linked to the company Objectives and Key Results [(OKRs)](/company/okrs/). As we build our ESG strategy, we will continue to add and update relevant ESG KPIs to the handbook.

## FAQ

**Q: Who can I contact for ESG-related questions?** 

A:  Senior Director, ESG/DRI: Stacy Cline - @slcline on GitLab.
Email: ESG@GitLab.com. 

**Q: Does GitLab calculate its carbon emissions?** 

A: Yes, GitLab completed its first greenhouse gas (GHG) inventory covering emissions from FY23. Please view the results of our inventory [here](https://about.gitlab.com/handbook/esg/#climate-action-and-greenhouse-ghg-emissions) and our third-party assurance letter [here](chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://about.gitlab.com/documents/GitLab_FY2023_Verification_Opinion.pdf). 

**Q: Has GitLab set a Science Based Target (SBT)?**

A: Not yet. GitLab completed its first [GHG inventory](https://about.gitlab.com/handbook/esg/#climate-action-and-greenhouse-ghg-emissions) and will use the results to inform a reduction plan and target.

**Q: Does GitLab track employment by gender and ethnicity?**

A: Yes, [view the most recent identity data](/company/culture/inclusion/identity-data/). 

**Q: Does GitLab set goals to increase diversity?**

A: Yes, GitLab publishes [people success KPIs](/handbook/people-group/people-success-performance-indicators/#executive-summary). 

**Q: How does GitLab define underrepresented groups?**

A: An underrepresented group describes a subset of a population that holds a smaller percentage within a significant subgroup than the subset holds in the general population. View the full definition [here](/company/culture/inclusion/#examples-of-select-underrepresented-groups). 

**Q: Is GitLab certified as a diverse supplier?**

A: GitLab is a publicly traded company (NASDAQ: GTLB) and is not defined as a diverse supplier and is unable to be certified as such, accordingly. Nevertheless, diversity, inclusion, and belonging (DIB) is a [core value](https://about.gitlab.com/handbook/values/) at GitLab. On a daily basis, we strive to keep our operations, employment practices, and supplier selection in line with [this value](https://about.gitlab.com/company/culture/inclusion/). GitLab's [DIB team](https://handbook.gitlab.com/job-families/people-group/diversity-inclusion-partner/) builds an environment where all team members feel a [sense of belonging](https://handbook.gitlab.com/job-families/people-group/diversity-inclusion-partner/), which results in a truly inclusive and welcoming work environment. Moreover, GitLab's Procurement team selects potential suppliers with [responsible sourcing and diversity](https://about.gitlab.com/handbook/finance/procurement/) in mind.

**Q: I'm a GitLab team member and I received an ESG questionnaire through an RFP. What do I do?**

A: Please follow the steps outlined in the [RFP process](https://about.gitlab.com/handbook/security/security-assurance/field-security/Field-Security-RFP.html).

**Q: Does GitLab sponsor/fund nonprofit organizations?**

A: GitLab does not currently have a formal corporate giving program. Pending the results of the 2022 materiality assessment, the first iteration of a corporate giving program is planned for 2023. 

**Q: Does GitLab donate its product to nonprofits?**

A: GitLab does not currently have a formal in-kind donation program. Pending the results of the 2022 materiality assessment, the first iteration of a non-profit in-kind donation program is planned for 2023. Learn about [other ways](/handbook/marketing/developer-relations/community-programs/education-program/) that GitLab contributes its product to the community. 



