---
layout: handbook-page-toc
title: Switchboard team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Summary

The Switchboard is a team within the [Dedicated Group](https://about.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/). We follow the same processes as listed on the [the Dedicated Group](https://about.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/), unless a difference exists which is explicitly noted on this page.


## Team Members

<%= direct_team(manager_slug: 'ashiel')%>

Product Manager: [Loryn Bortins](https://about.gitlab.com/company/team/#lbortins)

## Working with us

To engage with the Switchboard team:

- [Create an issue](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/new) in the GitLab Dedicated team issue tracker
- Label the issue with:
  - `component::Switchboard`
  - `workflow-infra::Triage`
  - `team::Switchboard`
- When creating an issue, it is not necessary to `@`mention anyone
- In case you want to get attention, use a specific team handle (Ex: @gitlab-dedicated/switchboard ) as defined in [Dedicated group hierarchy](https://about.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/#gitlab-group-hierarchy)
- [Switchboard issue board](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/boards/4498935?label_name[]=component%3A%3ASwitchboard) tracks all of the team's current work
- We are in the process of adapting to use the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/)

- Slack channels
  - For Switchboard specific questions, you can find us in [#g_dedicated-switchboard-team](https://gitlab.slack.com/archives/C04DG7DR1LG)
  - Other teams in Dedicated group have their own work channels for team work discussions:
      - [#g_dedicated-team](https://gitlab.slack.com/archives/C025LECQY0M)
      - [#g_dedicated-us-pubsec](https://gitlab.slack.com/archives/C03R5837WCV)

## Requesting Access to the Switchboard application

- Create an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) specifying
   - the specific environment ( Test / Beta / Production )
   - level of access required (Readonly, Support, Provisioner, Operator)
   - justification for the access
- Access & Provision Details for the application can be found in the `Switchboard - GitLab Dedicated` section of the [Tech Stack](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/d3561ab939029faf4ac25f32612c57e861eb8b39/data/tech_stack.yml)

## How we work

### Meetings and Scheduled Calls

Our preference is to work asynchronously, within our project issue tracker as described in [the project management section](https://about.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/#project-management).

The team does have a set of regular synchronous calls:

- `Switchboard Sync` - During this call, we are sharing important information for team-members day-to-day, as well as project items requiring a sync discussion
- 1-1s between the Individual Contributors and Engineering Managers

Impromptu Zoom meetings for discussing Switchboard work between individuals are created as needed.
It is expected that these meetings are private streamed, or recorded(1*), and then uploaded to [GitLab Unfiltered playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp3NBMl7c0DGXCjW5rSPeOK).
The outcome of the call is shared in a persistent location (Slack is not persistent). This is especially important as the team grows, because any decisions that are made in the early stage have will be questioned in the later stages when the team is larger.

`1*` Exceptions to the recording rule are: 1-1 calls, discussions around non-project work, and in cases where parties do not feel comfortable with recording or we cannot record due to the nature of content discussed. However, even with the exceptions, outcome of project related discussions need to be logged in a persistent location, such as the main issue tracker.

### Merge Request Review Guidelines

We specifically adhere to the [GitLab Code Review Guidelines](#gitlab-code-review-guidelines) and follow
the [Dedicated group principles](https://about.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/#merge-requests) when requesting merge request reviews.

#### Merge request review process

As the Switchboard team is currently small, we use an 'Approve and Merge' approach:

1. When you're ready to have your merge request reviewed, select a [Switchboard reviewer](https://gitlab.com/groups/gitlab-dedicated/switchboard/reviewers/-/group_members).
   * If you're not certain about who to choose, you can use the [reviewer roulette](#reviewer-roulette) to randomly select a reviewer.
1. The reviewer will perform a review based on [reviewing a merge request guidelines](https://docs.gitlab.com/ee/development/code_review.html#reviewing-a-merge-request).
1. If satisfied, the reviewer will approve.
1. If the merge request contains the required approvals, the reviewer will trigger a pipeline and set auto-merge.
   * If the reviewer does not have merge permission, they should seek out a maintainer for merging.

Notes:
- It is our intention to move towards a typical 'reviewers and maintainers' approach which would require two reviews as soon as we have the team members to support this.
- Merge requests should be approved based on the [approval guidelines](#approval_guidelines).
- As per the [GitLab Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) there are scenarios where it is appropriate for the author to merge the merge request: If there are no blocking comments, and the merge request has all the required approvals, the author or maintainer can merge.
- Switchboard project is configured to use [pipelines for merged results](https://docs.gitlab.com/ee/ci/pipelines/merged_results_pipelines.html) which means that reviewers need to run a pipeline pre-merge to guarantee that updates are compatible with the latest main branch.
- When reviewing merge requests, reviewers should use the [Conventional Comment labels](https://conventionalcomments.org/#labels) to convey your intent.
- We label merge requests using the [Specialization labels](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/labels/index.md#specialization-labels) found in the [GitLab documentation](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/labels/index.md). MRs should be labelled ~"frontend", ~"backend" or ~"documentation"

#### Approval guidelines

| If your merge request includes  | It must be approved by a |
| ------------------------------- | ------------------------ |
| `~backend` changes        | [Backend maintainer](https://gitlab.com/groups/gitlab-dedicated/switchboard/maintainers/-/group_members). |
| `~frontend` changes       | [Frontend maintainer](https://gitlab.com/groups/gitlab-dedicated/switchboard/maintainers/-/group_members). |

#### Reviewer roulette

Reviewer roulette is an internal tool for use on GitLab.com projects that randomly picks a maintainer for each area of the codebase. To select a maintainer:

1. Go to the [reviewer roulette](https://gitlab-org.gitlab.io/gitlab-roulette/?currentProject=switchboard&sortKey=stats.avg30&mode=show&order=-1) page.
1. Select the Switchboard project.
1. Choose the desire role: `~maintainer::frontend`, `~maintainer::backend`, `~reviewer::backend`, `~reviewer::frontend`.
1. Click on `Spin the wheel`.

#### GitLab Code Review Guidelines

- [Having your merge request reviewed](https://docs.gitlab.com/ee/development/code_review.html#having-your-merge-request-reviewed)
- [Reviewing a merge request](https://docs.gitlab.com/ee/development/code_review.html#reviewing-a-merge-request)
- [The Right Balance](https://docs.gitlab.com/ee/development/code_review.html#the-right-balance)
- [Quality](https://docs.gitlab.com/ee/development/code_review.html#quality)
- [Performance, reliability and availability](https://docs.gitlab.com/ee/development/code_review.html#performance-reliability-and-availability)
- [Merge request performance guidelines](https://docs.gitlab.com/ee/development/merge_request_concepts/performance.html)

### Reviewers and maintainers

There are two groups for Switchboard, [Reviewers and Maintainers](https://gitlab.com/gitlab-dedicated/switchboard):

* All Switchboard team members are included in the `Reviewer` group.
* When a team member is fully onboarded and feel confident in their knowledge of the codebase they are invited to the Maintainer group.

### Resources

[Switchboard Direction Page](https://about.gitlab.com/direction/saas-platforms/switchboard/)

