---
layout: handbook-page-toc
title: Product Leadership
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<%= partial("handbook/product/product-handbook-links") %>

## General Product Organizational Structure

The GitLab Product team includes team members at various levels of [Product Management job titles](/handbook/product/product-manager-role/product-CDF-competencies/) across our [organizational levels](/company/team/structure/#levels) with scope at various points in our [product hierarchy](/handbook/product/categories/#hierarchy). As a result there can be instances where peers across layers don't have the same title. We will always abide by [GitLab's layer structure](/company/team/structure/#layers).

| Level | Job Families | Hierarchy Scopes |
| ----- | ------------ | ---------------- |
| IC | [Product Manager](https://handbook.gitlab.com/job-families/product/product-manager/) | Group, Stage |
| Manager | [Group Manager Product](https://handbook.gitlab.com/job-families/product/product-management-leadership/#group-manager-product-gmp), [Director of Product](https://handbook.gitlab.com/job-families/product/product-management-leadership/) | Section |
| Senior Leader | [VP](https://handbook.gitlab.com/job-families/product/product-management-leadership/#vp-of-product) | All Sections |
| Executive | [Chief Product Officer](https://handbook.gitlab.com/job-families/product/chief-product-officer/) | Entire Function |

## Product Leaders

All Managers and above in the Product function are considered product leaders. They can be referenced using the `@gl-product-leadership` handle. This document describes important leadership concepts specific to the product management team. See also our page on [general GitLab leadership guidance](/handbook/leadership/).

### Product Leadership Team Structure

Product team members who report directly to the [Chief Product Officer](https://handbook.gitlab.com/job-families/product/chief-product-officer/) are members of the Product Leadership Team. 

This group can be referenced in GitLab.com issues using the `@gl-product-plt` handle.

## Product Group Conversation Leader Rotation

The Product [Group Conversation](https://about.gitlab.com/handbook/group-conversations/) will be hosted by a Product Stage Leader, Group Manager or a member of the [Product Leadership](https://about.gitlab.com/handbook/product/product-leadership/#product-leadership-team-structure) team. All members of the Product Leadership team are encouraged to host the Product Group Conversation. The Group Conversations will either reference a previous Product Key Review or Product Group Conversation presentation, depending on which is most recent. Details on how to drive the tasks will be in the [automatated issue](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Product-Group-Conversation.md) that will be assigned to the GC host 4 weeks prior to the actual GC date by the Product EBA. 

For more details, visit the Group Conversations [handbook page](https://about.gitlab.com/handbook/group-conversations/#additional-consideration-for-functional-presentations)

Product Section Leaders, Group Managers and Product leaders can sign up for a GC in the [sign up sheet](https://docs.google.com/spreadsheets/d/1hB_JNwI57BLvS2OFDsPSJGg32XTYpjAAm0DNnPLFQ3U/edit?usp=sharing).

| Date | Host | Planning Issue |
| ---- | ---- | -------------- |
| 2023-01-17 | Sarah Waldner (Dev) | [Issue 5268](https://gitlab.com/gitlab-com/Product/-/issues/5268) | 
| 2023-02-28 | Jackie Porter (Ops) | TBD |
| 2023-03-22 | Hillary Benson (Sec) | [Issue 5501](https://gitlab.com/gitlab-com/Product/-/issues/5501) |
| 2023-04-13 | Joshua Lambert (Enablement) | TBD |
| 2023-05-23 | Sam Kerr | https://gitlab.com/gitlab-com/Product/-/issues/5680 |
| 2023-08-03 | TBD | TBD |
| 2023-11-07 | TBD | TBD |


## Product Section Leaders Performance Indicator Rotation
In an effort to ensure cross-section understanding of performance indicators and activities to improve them, section leaders are [assigned another section on a quarterly basis](https://gitlab.com/gitlab-com/Product/-/main/.gitlab/issue_templates/Quarterly-Product-Leadership-PI-Pairing.md). Section leaders attend and investigate PIs for that section throughout the course of the quarter.

| Section | FY23Q2 Pair |
| ------- | ----------- |
| Dev | KennyJ |
| Sec | OmarF |
| Ops | HillaryB |
| Data Science | OmarF |
| Enablement | OritG |
| Fulfillment | SamA |
| Growth | JoshL |

## Product Leadership ReadMe's

Below you'll find the ReadMe's for our Product Division leaders who manage teams. 

- [David DeSanto's README](https://gitlab.com/david)
- [Justin Farris's README](https://gitlab.com/justinfarris)
- [Kevin Chu's README](https://gitlab.com/kbychu/README)
- [Sarah Waldner's README](https://gitlab.com/sarahwaldner/README)
- [Jackie Porter's README](https://gitlab.com/jreporter/read-me)
- [Omar Fernandez's README](https://gitlab.com/ofernandez2)

## Product Manager/Leader Collaboration

As a [product team leader](#product-leaders), it's important to set the tone for the organization.
We put our PMs individually in the forefront as
[Directly Responsible Individuals (DRI)](/handbook/people-group/directly-responsible-individuals/),
with the trust and power that they can make the right decisions with our support as
product leaders. This section is intended to outline some best practices for
working between PMs and their leaders. It contains guidance on responsibilities and
expectations the leader should have in working with PMs, but is not intended
to be hard and fast rules that take the place of having a strong working relationship
and prioritizing things effectively together.

**Note** - This is intended as a supplement to the [product director](job-families/product/product-management-leadership/) or [group manager of product](https://handbook.gitlab.com/job-families/product/product-management-leadership/#group-manager-product-gmp)
job descriptions with specific focus on the interaction between PMs and their managers.
General job responsibilities can be found at that link.

An effective product leader should:

- Respect that multiple voices influencing the group will be confusing to the
execution team, and avoid coming out of left field with changing priorities.
The PM owns the [day-to-day interaction](/handbook/product/product-processes/#working-with-your-group)
with their group and the leader should influence through the directly responsible
PM.
- Work through your PMs as your path to success, rather than focusing on individual
achievement. We have a culture of empowering individual PMs, not centralizing
authority, and need to continue to reinforce that as we grow.
- Help your PMs understand who [important internal and external customers](/handbook/product/product-processes/#sensing-mechanisms)
are for them, ensuring that there's a productive ongoing dialogue with feedback
coming in and then features being adopted internally. [Internal customers](/handbook/values/#dogfooding)
should have at least a monthly check-in, with the internal customer section
in the category epic kept up to date.
- Represent their portfolio in day-to-day interaction with [D](https://about.gitlab.com/company/team/structure/#director-group) and [E](https://about.gitlab.com/company/team/structure/#e-group) groups, with
particular responsibility to prioritize inputs from leadership into something
stable in the short/medium term and actionable by the PMs, to align efforts
across teams, and to raise visibility of wins by the PM to the broader team
by being a "cheerleader" for the successes of their reports to the senior
leadership group. This also includes publishing portfolio-level directional
items that the rest of the company can use (and which should be reviewed and
understood by the DRI PMs who will deliver this portfolio-level vision.)
- Support and hold PMs accountable for updates to the next release, 3 month vision, OKRs, etc.
For teams using web category vision epics, this can be done in a very nice
monthly update flow aligned to the releases and done via discussion in a comprehensive
MR. Whatever the process, though, the leader should be holding the PM accountable
by providing support and guidance here, particularly when it comes to making sure
organizational priorities are clear. Make sure upcoming portfolio-level milestones,
due dates around releases, special projects with calendar impacts, etc. are visible
and always clearly communicated.
- Actively coach your PMs, sensing where they can grow and do better. This
coaching should be provided consistently, clearly, and in an actionable way, with
the intent of avoiding "surprise problems". In the same vein, following up on
requests for context, advice, and so on from the PMs is a priority. The
[socratic method](/company/team/structure/#management-group) is
recommended as a great approach that works particularly well with PMs.
- Prioritize hiring, being sure to include PMs (and EMs/team members) who will work
with the new person in the process.
- Provide structure and motivation for needed organizational changes (being more
[data-driven](/handbook/product/#data-driven-work),
telling stories, providing time for expansive thinking).
